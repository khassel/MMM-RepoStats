# MMM-RepoStats

This is a module for the [MagicMirror²-Project](https://github.com/MichMich/MagicMirror/).

It displays statistics of following repository types:

- Docker Images **or**
- GitHub Repositories **or**
- GitLab Repositories

You can run one instance of this module for every type.

## Examples

### All types with public data

![](img/example_3_types.jpg)

### Gitlab including private data (token needed)

![](img/gitlab_with_token.jpg)

## Installation

Assuming `~/MagicMirror` is the directory where you installed MagicMirror².

### Clone and install

```bash
cd ~/MagicMirror/modules
git clone https://gitlab.com/khassel/MMM-RepoStats.git
cd MMM-RepoStats
npm install
```

### Update your config.js file

Add a configuration block to the modules array in the `~/MagicMirror/config/config.js` file. The following configuration contains the first example, where 3 instances of this module are configured:

```js
var config = {
  modules: [
    {
      module: "MMM-RepoStats",
      position: "top_left",
      config: {
        repoList: [
          "karsten13/magicmirror",
          "karsten13/mmpm",
          "library/node",
          "library/traefik",
        ],
      },
    },
    {
      module: "MMM-RepoStats",
      position: "top_left",
      config: {
        type: "gitlab",
        title: "GitLab",
        repoList: [
          "khassel/magicmirror", 
          "khassel/mmpm",
        ],
      },
    },
    {
      module: "MMM-RepoStats",
      position: "top_left",
      config: {
        type: "github",
        title: "GitHub",
        repoList: [
          "MagicMirrorOrg/MagicMirror", 
          "Bee-Mar/mmpm",
        ],
      },
    },
  ],
};
```

Configuration options:

| Key               | Default value                               | Possible values              | Description                                                                                                                                            |
| ----------------- | ------------------------------------------- | ---------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------ |
| type              | "docker"                                    | "docker", "github", "gitlab" | repository type                                                                                                                                        |
| token             | ""                                          |                              | gitlab token needed for private data                                                                                                                   |
| title             | "Docker Images"                             |                              | title displayed                                                                                                                                        |
| repoList          | ["karsten13/magicmirror", "karsten13/mmpm"] |                              | list of repositories (hint: if you want to use official docker images as e.g. `node`, you have to prefix them with `library/`, see also example above) |
| shortenBigNumbers | true                                        | true, false                  | shorten big numbers, e.g. display 1K+ instead of 1234 and 234M+ instead of 234234234                                                                   |
| updateInterval    | 15 * 60 * 1000                              | number of milliseconds       | interval getting new data                                                                                                                              |
