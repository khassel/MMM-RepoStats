## Start Containers

```bash
cd /mnt/z/k13/MMM-RepoStats/

docker run --rm -it -u root -v $(pwd)/debug:/opt/magic_mirror/config -v $(pwd):/opt/magic_mirror/modules/MMM-RepoStats -p 8080:8080 karsten13/magicmirror:develop bash
```