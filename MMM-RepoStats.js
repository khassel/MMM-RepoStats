/**
 * @file MMM-RepoStats.js
 *
 * @author Karsten Hassel
 * @license MIT
 *
 * @see  https://gitlab.com/khassel/MMM-RepoStats
 */

/**
 * @external Module
 * @see https://github.com/MichMich/MagicMirror/blob/master/js/module.js
 */

/**
 * @external Log
 * @see https://github.com/MichMich/MagicMirror/blob/master/js/logger.js
 */

/**
 * @module MMM-RepoStats
 * @description Frontend for the module to display data.
 *
 * @requires external:Module
 * @requires external:Log
 */
Module.register("MMM-RepoStats", {
  repoList: [],
  resCounter: 0,
  showPipeline: false,

  defaults: {
    type: "docker", // possible types are "docker", "github" or "gitlab"
    token: "", // not needed, only available for types "github" or "gitlab", with token more properties can be fetched
    title: "Docker Images", // title displayed
    repoList: ["karsten13/magicmirror", "karsten13/mmpm"], // array of repositories
    shortenBigNumbers: true, // display 1K+ instead of 1234 and 234M+ instead of 234234234
    dockerStatsRepo: "", // special case, leave empty
    zoom: 12,
    width: 600,
    height: 600,
    updateInterval: 15 * 60 * 1000, // every 15 minutes
  },

  getStyles() {
    return ["font-awesome.css", "MMM-RepoStats.css"];
  },

  getTemplate() {
    return "templates/" + this.config.type + ".njk";
  },

  getTemplateData() {
    switch (this.config.type) {
      case "gitlab":
        return {
          config_gitlab: this.config,
          repoList_gitlab: this.repoList,
          showPipeline_gitlab: this.showPipeline,
        };
      case "github":
        return {
          config_github: this.config,
          repoList_github: this.repoList,
          // showPipeline_github: this.showPipeline
        };
      default:
        return {
          config_docker: this.config,
          repoList_docker: this.repoList,
        };
    }
  },

  start() {
    Log.info(`Starting module: ${this.name} ${this.config.type}`);
    this.config.repoList.forEach((repo) => {
      switch (this.config.type) {
        case "gitlab":
          this.repoList.push({
            name: repo,
            forks: undefined,
            stars: undefined,
          });
          break;
        case "github":
          this.repoList.push({
            name: repo,
            forks: undefined,
            stars: undefined,
            subscriber: undefined,
            issues: undefined,
          });
          break;
        default:
          this.repoList.push({
            name: repo,
            count: undefined,
            stars: undefined,
          });
      }
    });
    this.sendSocketNotification(
      "REPOSITORY_CONFIG_" + this.config.type,
      this.config
    );
  },

  findRepo(name) {
    for (var i = 0; i < this.repoList.length; i++) {
      if (this.repoList[i]["name"] === name) {
        return this.repoList[i];
      }
    }
    return null;
  },

  updateList(payload) {
    ++this.resCounter;
    const obj = JSON.parse(payload);
    const repo = this.findRepo(obj.name);
    if (repo != null) {
      repo.stars = obj.stars;
      repo.updated = obj.updated;
      switch (this.config.type) {
        case "gitlab":
          repo.forks = obj.forks;
          if (this.config.token !== "") {
            repo.public = obj.public;
            repo.stats = obj.stats;
            repo.issues = obj.issues;
            repo.branch = obj.branch;
            repo.run = obj.run;
            repo.result = obj.result;
            if (repo.branch != null) {
              this.showPipeline = true;
            }
          }
          break;
        case "github":
          repo.forks = obj.forks;
          repo.subscriber = obj.subscriber;
          repo.issues = obj.issues;
          break;
        default:
          repo.count = obj.count;
          if (this.config.dockerStatsRepo !== "") {
            repo.dayly = obj.dayly;
            repo.avgdayly = obj.avgdayly;
          }
      }
    }
    if (this.resCounter === this.repoList.length) {
      this.resCounter = 0;
      this.updateDom(300);
    }
  },

  socketNotificationReceived(notification, payload) {
    if (notification === "REPOSITORY_STATS_" + this.config.type) {
      this.updateList(payload);
    }
  },
});
