/**
 * @file node_helper.js
 *
 * @author Karsten Hassel
 * @license MIT
 *
 * @see  https://gitlab.com/khassel/MMM-RepoStats
 */

const NodeHelper = require("node_helper");
const mmObj = require("magicmirror-helper/mm_helper");
const { formatDistanceStrict } = require("date-fns/formatDistanceStrict");
const Log = require("logger");

module.exports = NodeHelper.create(
  Object.assign({}, mmObj, {
    start() {
      Log.log(`Starting module helper: ${this.name}`);
    },

    socketNotificationReceived(notification, payload) {
      if (notification.match(/REPOSITORY_CONFIG_.*/)) {
        this.addPayload(payload.type, payload);
        this.getData(payload);
      }
    },

    async getData(configPayload) {
      const sleep = (delay) => new Promise((resolve) => setTimeout(resolve, delay));
      await sleep(1000);

      try {
        configPayload.repoList.forEach((repo) => {
          const logError = (e) => {
            Log.error(`${this.name} ${configPayload.type}: ${e}`);
          };

          const formatDate = (data) => {
            return formatDistanceStrict(new Date(data), new Date());
          };

          const formatNumber = (nr) => {
            if (!configPayload.shortenBigNumbers) return nr;
            if (nr < 1000) return nr;
            if (nr < 1000000) return Math.trunc(nr / 1000) + "K+";
            if (nr < 1000000000) return Math.round(nr / 100000) / 10 + "M+";
            return Math.round(nr / 100000000) / 10 + "B+";
          };

          const sendJson = (jsonList) => {
            // Log.dir(jsonList.json());
            this.sendSocketNotification(
              "REPOSITORY_STATS_" + configPayload.type,
              jsonList.json()
            );
          };

          const addGithub = async (jsonList) => {
            try {
              const data = await this.fetchJson(
                "https://api.github.com/repos/" + repo,
                { headers: { "USER-AGENT": "Magic Mirror" } }
              );
              jsonList.add("name", repo);
              jsonList.add("forks", formatNumber(data.forks));
              jsonList.add("stars", formatNumber(data.stargazers_count));
              jsonList.add("subscriber", formatNumber(data.subscribers_count));
              jsonList.add("issues", formatNumber(data.open_issues));
              jsonList.add("updated", formatDate(data.updated_at));
            } catch (e) {
              logError(e);
            }
            sendJson(jsonList);
          };

          const addDocker = async (jsonList) => {
            try {
              const data = await this.fetchJson(
                "https://hub.docker.com/v2/repositories/" + repo + "/",
                {}
              );
              jsonList.add("name", repo);
              jsonList.add("count", formatNumber(data.pull_count));
              jsonList.add("countnumber", data.pull_count);
              jsonList.add("stars", formatNumber(data.star_count));
              jsonList.add("updated", formatDate(data.last_updated));
              if (configPayload.dockerStatsRepo !== "") {
                try {
                  const data = await this.fetchText(
                    "https://gitlab.com/api/v4/projects/" +
                      encodeURIComponent(configPayload.dockerStatsRepo) +
                      "/repository/files/" +
                      repo.replace(/.*\//g, "") +
                      ".log/raw?ref=master",
                    { headers: { "PRIVATE-TOKEN": configPayload.token } }
                  );
                  const lines = data.split(/\r?\n/);
                  let i = lines.length;
                  if (i > 1) {
                    if (i > 10) i = 10;
                    const nr_one = parseInt(lines[0].split(" ")[1]);
                    const nr_ten = parseInt(lines[i - 1].split(" ")[1]);
                    const obj = JSON.parse(jsonList.json());
                    const dayly = parseInt(obj.countnumber) - nr_one;
                    const avgdayly = Math.round((nr_one - nr_ten) / i);
                    jsonList.add("dayly", formatNumber(dayly));
                    jsonList.add("avgdayly", formatNumber(avgdayly));
                  }
                } catch (e) {
                  logError(e);
                }
              }
            } catch (e) {
              logError(e);
            }
            sendJson(jsonList);
          };

          const addGitlab = async (jsonList) => {
            if (configPayload.token !== "") {
              fetchOptions = {
                headers: { "PRIVATE-TOKEN": configPayload.token },
              };
            }

            try {
              const data = await this.fetchJson(
                "https://gitlab.com/api/v4/projects/" +
                  encodeURIComponent(repo),
                fetchOptions
              );
              jsonList.add("name", repo);
              jsonList.add("forks", formatNumber(data.forks_count));
              jsonList.add("stars", formatNumber(data.star_count));
              jsonList.add("updated", formatDate(data.last_activity_at));
              let pub = "";
              if (data.visibility === "public") pub = "X";
              jsonList.add("public", pub);
              jsonList.add("issues", formatNumber(data.open_issues_count));
              if (configPayload.token !== "") {
                jsonList.hasPipelines = data.builds_access_level !== "disabled";
                // get Stats
                try {
                  const data = await this.fetchJson(
                    "https://gitlab.com/api/v4/projects/" +
                      encodeURIComponent(repo) +
                      "/statistics",
                    fetchOptions
                  );
                  jsonList.add("stats", formatNumber(data.fetches.total));
                  // get pipeline
                  if (jsonList.hasPipelines) {
                    try {
                      const data = await this.fetchJson(
                        "https://gitlab.com/api/v4/projects/" +
                          encodeURIComponent(repo) +
                          "/pipelines",
                        fetchOptions
                      );
                      if (data[0] != null) {
                        jsonList.add("branch", data[0].ref);
                        jsonList.add("run", formatDate(data[0].updated_at));
                        jsonList.add("result", data[0].status);
                      }
                    } catch (e) {
                      logError(e);
                    }
                  }
                } catch (e) {
                  logError(e);
                }
              }
            } catch (e) {
              logError(e);
            }
            sendJson(jsonList);
          };

          let jsonList = {
            hasPipelines: false,
            list: [],
            add: function (key, value) {
              this.list.push('"' + key + '": "' + value + '"');
            },
            json: function () {
              let res = "";
              this.list.forEach((elem) => (res = res + elem + ","));
              return (res = "{" + res.substring(0, res.length - 1) + "}");
            },
          };
          let fetchOptions = {};
          switch (configPayload.type) {
            case "gitlab":
              addGitlab(jsonList);
              break;
            case "github":
              addGithub(jsonList);
              break;
            default:
              addDocker(jsonList);
          }
        });
      } catch (e) {
        logError(e);
      }
    },
  })
);
